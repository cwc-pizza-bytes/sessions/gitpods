# Why GitPods?

GitPods allow us to develop in the browser without having to install anything locally. 

There are loads of resources online for how it works, so this is more focussed around how to use it (for us!).

You need the following to get started:
1. A gitpod.io account

How to develop in the browser:
1. In your code repo, make sure there is a `.gitpod.yml` file with an `image:` key. This should point to a built docker image, e.g. `rust` `ubuntu`. Or our own Pico SDK image available here: `registry.gitlab.com/cwc-pizza-bytes/development-environment/pico-idk/snapshot:main`
1. Log into `gitpod.io`.
1. Open up your repo with `gitpod.io/#` infront of the url. e.g. `gitpod.io/#https://gitlab.com/cwc-pizza-bytes/sessions/gitpods`.
1. Wait ... (you might need to refresh the page, or, in a separate tab navigate to `gitpod.io`).

How to create a new development image:
1. Copy [this repo](https://gitlab.com/cwc-pizza-bytes/development-environment/pico-idk) but change the dockerfile to your own installation. 
